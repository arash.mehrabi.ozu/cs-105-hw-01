package cs105;

import java.util.Scanner;

public class Q3 {
    public static int reverseInteger(int number) {
        int length = (int) (Math.log10(number) + 1);
        int result = 0;

        for (int i = length - 1; i >= 0; --i) {
            result += (number % 10) * Math.pow(10, i);
            number /= 10;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.print("Please enter the integer that you want to reverse: ");

        Scanner sc = new Scanner(System.in);
        int integerToBeReversed = sc.nextInt();

        System.out.printf("The reversed version of the integer %d is %d.", integerToBeReversed, reverseInteger(integerToBeReversed));
    }
}
