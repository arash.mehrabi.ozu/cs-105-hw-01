package cs105;

import java.util.Scanner;

public class Q4 {

    public static boolean isPrime(int number) {
        for (int i = 2; i < number / 2 + 1; ++i) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.print("Please enter the integer that you want to see if it is prime: ");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

        System.out.printf("The integer %d is prime? %b.", number, isPrime(number));
    }
}
