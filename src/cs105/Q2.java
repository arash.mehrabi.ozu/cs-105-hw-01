package cs105;

import java.util.Scanner;

public class Q2 {

    public static double estimatePi(int numberOfTerms) {
        double estimatedPi = 0.0;
        for (int k = 0; k < numberOfTerms; ++k) {
            estimatedPi += Math.pow(-1, k + 2) / (2 * (k + 1) - 1);
        }
        return estimatedPi * 4;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter the number of terms: ");
        int numberOfTerms = sc.nextInt();

        double estimated_pi = estimatePi(numberOfTerms);

        System.out.printf("For %d terms, estimated pi is %.6f, and the ratio of estimated pi / pi is %.6f. ", numberOfTerms, estimated_pi,
                estimated_pi / Math.PI);
    }
}
