package cs105;

import java.util.Scanner;

public class Q1 {
    public static double getAreaOfRegularPentagon(double perimeter, double apothem) {
        return perimeter * apothem / 2;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter the perimeter of a regular pentagon: ");
        double perimeter = sc.nextDouble();

        System.out.print("Please enter the apothem of a regular pentagon: ");
        double apothem = sc.nextDouble();

        System.out.printf("The area of the given regular pentagon is %.2f", getAreaOfRegularPentagon(perimeter, apothem));
    }
}
